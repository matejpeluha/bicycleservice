#ifndef ADDCOMPONENTDIALOG_H
#define ADDCOMPONENTDIALOG_H

#include <QDialog>

namespace Ui {
class AddComponentDialog;
}

class AddComponentDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddComponentDialog(QWidget *parent = nullptr);
    ~AddComponentDialog();

    QString getComponent();
    double getPrice();
    void closeEvent(QCloseEvent *event);
private:
    Ui::AddComponentDialog *ui;

signals:
    void addComponentSignal(QString componentName, double price);
    void closeSignal();
private slots:
    void on_pushButton_clicked();
};

#endif // ADDCOMPONENTDIALOG_H
