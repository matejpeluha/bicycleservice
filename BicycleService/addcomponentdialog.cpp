#include "addcomponentdialog.h"
#include "ui_addcomponentdialog.h"

#include <QCloseEvent>
#include <QMessageBox>

AddComponentDialog::AddComponentDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddComponentDialog)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::WindowCloseButtonHint);
}

AddComponentDialog::~AddComponentDialog()
{
    delete ui;
}

QString AddComponentDialog::getComponent(){
    return this->ui->componentInput->text();
}

double AddComponentDialog::getPrice(){
    return this->ui->componentPriceInput->value();
}

void AddComponentDialog::on_pushButton_clicked()
{
    QString componentName = this->ui->componentInput->text();
    if(componentName.isEmpty()){
        QMessageBox::warning(this, "Pridanie komponentu", "Názov komponentu musí byť vyplnený!");
        return;
    }

    double componentPrice = this->ui->componentPriceInput->value();

    emit addComponentSignal(componentName, componentPrice);
}

void AddComponentDialog::closeEvent (QCloseEvent *event)
{
        emit closeSignal();
        event->accept();
}
