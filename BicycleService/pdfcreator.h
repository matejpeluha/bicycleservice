#ifndef PDFCREATOR_H
#define PDFCREATOR_H

#include "adddialog.h"



class PdfCreator
{
public:
    PdfCreator();
    void createPdf(AddDialog& addDialog, QString filePath);
private:
    void fillHtml(AddDialog &addDialog, QString &html);
    void fillDate(QString &html, QString& date);
    void fillSender(QString &html);
    void fillTitle(QString &html, QString &title);
    void fillContent(AddDialog &addDialog, QString &html);
};

#endif // PDFCREATOR_H
