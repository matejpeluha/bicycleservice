#ifndef DATABASEHANDLER_H
#define DATABASEHANDLER_H

#include "adddialog.h"

#include <QFile>
#include <QTableWidget>



class DatabaseHandler
{
public:
    DatabaseHandler(QTableWidget* table);

    void loadData(const QString &nameFilter = "", const QString &serviceFilter = "");
    void closeDatabase();

    void appendRow(AddDialog& addDialog);
    void removeRow(int rowIndex);
    void openDatabase(QIODevice::OpenMode mode);

    void loadComponents(AddDialog &addDialog, int rowIndex);
    void editRow(AddDialog &addDialog);
    void createBackUp();

private:
    QTableWidget* table;

    QString dataPath;
    QFile* csvFile;
    QTextStream* csvStream;


    const int ID_COLUMN_INDEX = 0;
    const int NAME_COLUMN_INDEX = 1;
    const int PHONE_COLUMN_INDEX = 2;
    const int ADRESS_COLUMN_INDEX = 3;
    const int BICYCLE_TYPE_COLUMN_INDEX = 4;
    const int BICYCLE_NUMBER_COLUMN_INDEX = 5;
    const int SERVICE_COLUMN_INDEX = 6;
    const int DATE_COLUMN_INDEX = 7;
    const int PRICE_COLUMN_INDEX = 8;
    const int DETAIL_COLUMN_INDEX = 9;
    const int REMOVE_COLUMN_INDEX = 10;

    const int COMPONENT_COLUMN_INDEX = 0;
    const int COMPONENT_PRICE_COLUMN_INDEX = 1;
    const int COMPONENT_REMOVE_COLUMN_INDEX = 2;

    void writeLineToTable(QString &line);
    void writeInfoToTableCell(QString &line, int columnIndex);
    void setRemoveColumn(int rowIndex);
    void setDetailColumn(int rowIndex);
    void loadComponentsToTable(AddDialog &addDialog, QString &components);

    void addEditedRow(AddDialog &addDialog, QString &newContent);
};

#endif // DATABASEHANDLER_H
