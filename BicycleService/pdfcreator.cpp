#include "pdfcreator.h"

#include <QTextDocument>
#include <QPrinter>

PdfCreator::PdfCreator()
{

}

void PdfCreator::createPdf(AddDialog &addDialog, QString filePath)
{

    QString html;
    fillHtml(addDialog, html);

    QTextDocument document;
    document.setHtml(html);

    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A4);
    printer.setOutputFileName(filePath);
    printer.setPageMargins(QMarginsF(15, 15, 15, 15));

    document.print(&printer);

}


void PdfCreator::fillHtml(AddDialog &addDialog, QString& html){
    QString date = addDialog.getDate();
    fillDate(html, date);

    //fillSender(html);

    QString title = "Záznam " + addDialog.getId();
    fillTitle(html, title);

    fillContent(addDialog, html);
}

void PdfCreator::fillDate(QString& html, QString& date){
    html +=
        "<div align=right>"
           + date +
        "</div>";
}

void PdfCreator::fillSender(QString& html){
    html +=
            "<div align=left>"
               "Sender Name<br>"
               "street 34/56A<br>"
               "121-43 city"
            "</div>";
}

void PdfCreator::fillTitle(QString& html, QString& title){
    html += "<h1 align=center>" + title + "</h1>";
}

void PdfCreator::fillContent(AddDialog& addDialog, QString& html){
    html += "<p align=justify style='font-size: 10px;'>";
    html += "<span><b>Meno: </b>" + addDialog.getName() + "</span><br>";
    html += "<span><b>Telefónne číslo: </b>" + addDialog.getPhoneNumber() + "</span><br>";
    html += "<span><b>Adresa: </b>" + addDialog.getAdress() + "</span><br>";
    html += "<span><b>Typ bicykla: </b>" + addDialog.getBicycleType() + "</span><br>";
    html += "<span><b>Výrobné číslo bicykla: </b>" + addDialog.getBicycleNumber() + "</span><br>";
    html += "<span><b>Servis: </b>" + addDialog.getService() + "</span><br>";
    html += "<div>";
    html += "<style>table,td,th{border: 1px solid black; border-collapse: collapse;}";
    html += "td,th{padding: 7px}</style>";
    html += "<table>";
    html+= "<tr>"
        "<th>Počet kusov</th>"
        "<th>Súčiastka</th>"
        "<th>Cena za kus</th>"
      "</tr>";

    const QTableWidget& table = addDialog.getComponentsTable();

    for(int rowIndex = 0; rowIndex < table.rowCount(); rowIndex++){
        QSpinBox* spinBox = (QSpinBox*)table.cellWidget(rowIndex, 0);
        html+= "<tr>"
            "<td>" + QString::number(spinBox->value()) + "ks</td>"
            "<td>" + table.item(rowIndex, 1)->text() + "</td>"
            "<td>" + table.item(rowIndex, 2)->text() + "</td>"
          "</tr>";
    }

    html += "</table></div><br>";
    html += "<span><b>Cena za prácu: </b>" + addDialog.getManWorkPrice() + "</span><br><br>";
    html += "<span style='font-size: 16px;'>Celková cena: " + addDialog.getPrice() + "</span><br><br>";
    html += "</p>";
}
