#include "adddialog.h"
#include "pdfcreator.h"
#include "ui_adddialog.h"

#include <QCloseEvent>
#include <QDate>
#include <QDebug>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>


AddDialog::AddDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDialog)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::WindowCloseButtonHint);

    setColumnSizes();

    this->ui->table->setSelectionBehavior(QAbstractItemView::SelectRows);
    this->ui->table->setFocusPolicy(Qt::NoFocus);

    this->manWorkPrice = 0;
    this->totalPrice = 0;

    setDate();
}

AddDialog::~AddDialog()
{
    delete ui;
}

void AddDialog::setColumnSizes(){
    QTableWidget* table = this->ui->table;
    QHeaderView* horizontalHeader = table->horizontalHeader();

    horizontalHeader->setSectionResizeMode(this->COUNT_COLUMN_INDEX, QHeaderView::Fixed);
    table->setColumnWidth(this->COUNT_COLUMN_INDEX, 90);

    horizontalHeader->setSectionResizeMode(this->COMPONENT_COLUMN_INDEX, QHeaderView::Stretch);

    horizontalHeader->setSectionResizeMode(this->PRICE_COLUMN_INDEX, QHeaderView::Fixed);
    table->setColumnWidth(this->PRICE_COLUMN_INDEX, 150);

    horizontalHeader->setSectionResizeMode(this->REMOVE_COLUMN_INDEX, QHeaderView::Fixed);
    table->setColumnWidth(this->REMOVE_COLUMN_INDEX, 90);
}

void AddDialog::setDate(){
    QDate date = QDate::currentDate();
    int day;
    int month;
    int year;
    date.getDate(&year, &month, &day);

    QString dateText = QString::number(day) + "." + QString::number(month) + "."  + QString::number(year);
    this->ui->dateLabel->setText(dateText);
}

void AddDialog::on_addComponentButton_clicked()
{
    this->addComponentDialog = new AddComponentDialog(this);

    connect(this->addComponentDialog, SIGNAL(addComponentSignal(QString, double)), this, SLOT(addComponentSlot(QString, double)));
    connect(this->addComponentDialog, SIGNAL(closeSignal(void)), this, SLOT(deleteAddComponentDialog()));

    this->addComponentDialog->show();
}

void AddDialog::addComponentSlot(QString componentName, double componentPrice){
    deleteAddComponentDialog();

    addComponent(componentName, componentPrice);
}

void AddDialog::addComponent(QString& componentName, double componentPrice){
    int numberOfRows = this->ui->table->rowCount();
    this->ui->table->setRowCount(numberOfRows + 1);

    setComponentColumn(numberOfRows, componentName);
    setCountColumn(numberOfRows);
    setPriceColumn(numberOfRows, componentPrice);
    setRemoveColumn(numberOfRows);

    this->totalPrice += componentPrice;
    showTotalPrice();
}

void AddDialog::addComponent(int componentCount, QString& componentName, double componentPrice){
    int numberOfRows = this->ui->table->rowCount();
    this->ui->table->setRowCount(numberOfRows + 1);

    setComponentColumn(numberOfRows, componentName);
    setCountColumn(numberOfRows, componentCount);
    setPriceColumn(numberOfRows, componentPrice);
    setRemoveColumn(numberOfRows);

    this->totalPrice += componentCount * componentPrice;
    showTotalPrice();
}

void AddDialog::deleteAddComponentDialog(){
    delete this->addComponentDialog;
}

void AddDialog::setComponentColumn(int rowIndex, QString componentName){
    QTableWidgetItem* item = new QTableWidgetItem(componentName);
    item->setFlags(item->flags() & (~Qt::ItemIsEditable));
    this->ui->table->setItem(rowIndex, this->COMPONENT_COLUMN_INDEX, item);
}

void AddDialog::setCountColumn(int rowIndex, int value){
    QSpinBox* spinBox = new QSpinBox();
    spinBox->setMinimum(1);
    spinBox->setMaximum(1000);
    spinBox->setValue(value);
    this->ui->table->setCellWidget(rowIndex, this->COUNT_COLUMN_INDEX, spinBox);

    connect(spinBox, SIGNAL(valueChanged(int)), this, SLOT(valueChangedSlot()));
}

void AddDialog::valueChangedSlot(){
    QTableWidget* table = this->ui->table;
    QSpinBox* spinBox;
    int componentCount;
    double componentPrice;
    this->totalPrice = this->ui->manWorkPriceInput->value();

    for(int index = 0; index < table->rowCount(); index++){
        spinBox = (QSpinBox*)table->cellWidget(index, this->COUNT_COLUMN_INDEX);
        componentCount = spinBox->value();
        componentPrice = table->item(index, this->PRICE_COLUMN_INDEX)->text().remove("€").toDouble();
        this->totalPrice += componentCount * componentPrice;
    }

    showTotalPrice();
}

void AddDialog::setPriceColumn(int rowIndex, double price){
    QString priceString = QString::number(price) + "€";
    QTableWidgetItem* item = new QTableWidgetItem(priceString);
    item->setFlags(item->flags() & (~Qt::ItemIsEditable));
    item->setTextAlignment(Qt::AlignCenter);
    this->ui->table->setItem(rowIndex, this->PRICE_COLUMN_INDEX, item);
}


void AddDialog::setRemoveColumn(int rowIndex){
    QIcon icon;
    QSize sz(16, 16);

    icon.addPixmap(style()->standardIcon(QStyle::SP_TrashIcon).pixmap(sz), QIcon::Normal);

    QTableWidgetItem *iconItem = new QTableWidgetItem;
    iconItem->setText("Zmazať");
    iconItem->setIcon(icon);
    iconItem->setFlags(iconItem->flags() & (~Qt::ItemIsEditable));

    this->ui->table->setItem(rowIndex, this->REMOVE_COLUMN_INDEX, iconItem);
}

void AddDialog::on_table_cellDoubleClicked(int row, int column)
{
    if(column == this->REMOVE_COLUMN_INDEX)
        removeRow(row);
}

void AddDialog::removeRow(int rowIndex){
    QString componentText = this->ui->table->item(rowIndex, this->PRICE_COLUMN_INDEX)->text().remove("€");
    double componentPrice = componentText.toDouble();
    QSpinBox* spinBox = (QSpinBox*)this->ui->table->cellWidget(rowIndex, this->COUNT_COLUMN_INDEX);
    int componentCount = spinBox->value();

    this->totalPrice -= componentCount * componentPrice;
    showTotalPrice();

    this->ui->table->removeRow(rowIndex);
}


void AddDialog::on_manWorkPriceInput_valueChanged(double newManWorkPrice)
{
    double difference = newManWorkPrice - this->manWorkPrice;
    this->manWorkPrice = newManWorkPrice;
    this->totalPrice += difference;

    showTotalPrice();
}

void AddDialog::showTotalPrice(){
    QString priceString = QString::number(this->totalPrice) + "€";
    this->ui->totalPriceLabel->setText(priceString);
}

void AddDialog::on_confirmButton_clicked()
{
    QString name = this->ui->nameInput->text();
    if(name.isEmpty()){
        QMessageBox::warning(this, "Pridanie záznamu", "Meno zákazníka musí byť vyplnené!");
        return;
    }

    QString bicycleType = this->ui->bicycleTypeInput->text();
    if(bicycleType.isEmpty()){
        QMessageBox::warning(this, "Pridanie záznamu", "Typ bicykla musí byť vyplnený!");
        return;
    }

    QString bicycleNumber = this->ui->bicycleNumberInput->text();
    if(bicycleNumber.isEmpty()){
        QMessageBox::warning(this, "Pridanie záznamu", "Výrobné číslo bicykla musí byť vyplnený!");
        return;
    }

    QString service = this->ui->serviceInput->text();
    if(service.isEmpty()){
        QMessageBox::warning(this, "Pridanie záznamu", "Servis musí byť vyplnený!");
        return;
    }

    QString id = this->ui->idLabel->text();
    emit finishSignal(id);
}


void AddDialog::closeEvent (QCloseEvent *event)
{
        emit closeSignal();
        event->accept();
}


QString AddDialog::getName(){
    return this->ui->nameInput->text();
}

QString AddDialog::getPhoneNumber()
{
    return this->ui->phoneInput->text();
}

QString AddDialog::getAdress(){
    return this->ui->adressInput->text();
}

QString AddDialog::getBicycleType()
{
    return this->ui->bicycleTypeInput->text();
}

QString AddDialog::getBicycleNumber(){
    return this->ui->bicycleNumberInput->text();
}

QString AddDialog::getService()
{
    return this->ui->serviceInput->text();
}

QString AddDialog::getPrice(){
    return this->ui->totalPriceLabel->text();
}

QString AddDialog::getDate()
{
    return this->ui->dateLabel->text();
}

QString AddDialog::getId()
{
    return this->ui->idLabel->text();
}

QString AddDialog::getManWorkPrice(){
    double manWorkPrice = this->ui->manWorkPriceInput->value();

    return QString::number(manWorkPrice) + "€";
}

void AddDialog::setId(int id)
{
    this->ui->idLabel->setText(QString::number(id));
}

void AddDialog::setLabel(const QString& labelName){
    this->ui->label->setText(labelName);
}

void AddDialog::setName(const QString& name){
    this->ui->nameInput->setText(name);
}

void AddDialog::setPhoneNumber(const QString &phoneNumber)
{
 this->ui->phoneInput->setText(phoneNumber);
}

void AddDialog::setAdress(const QString &adress)
{
 this->ui->adressInput->setText(adress);
}

void AddDialog::setBicycleType(const QString &bicycleType)
{
    this->ui->bicycleTypeInput->setText(bicycleType);
}

void AddDialog::setBicycleNumber(const QString &bicycleNumber)
{
    this->ui->bicycleNumberInput->setText(bicycleNumber);
}

void AddDialog::setService(const QString &service)
{
    this->ui->serviceInput->setText(service);
}

void AddDialog::setPrice(const QString &price)
{
    this->ui->totalPriceLabel->setText(price);
}

void AddDialog::setDate(const QString &date)
{
    this->ui->dateLabel->setText(date);
}

void AddDialog::setManWorkPrice(QString& manWorkPrice){
    double price = manWorkPrice.remove("€").toDouble();
    this->ui->manWorkPriceInput->setValue(price);
}

QTableWidget& AddDialog::getComponentsTable(){
    return *this->ui->table;
}

void AddDialog::addTotalPrice(QString& price){
    bool ok;
    double priceNumber = price.remove("€").toDouble(&ok);

    if(ok)
        this->totalPrice += priceNumber;
}

void AddDialog::on_pdfButton_clicked()
{
    QString pdfFileName = QInputDialog::getText(this, "Uloženie PDF", "Zapíš názov PDF súboru, pod ktorým ho cheš uložiť.");
    if(pdfFileName == "")
        return;

    QMessageBox::information(this, "Uloženie PDF", "PDF je nazvané " + pdfFileName + ".\nTeraz vyberieš pričinok, kde chceš uložiť PDF.");

    QString folder = QFileDialog::getExistingDirectory(this, "Vyber priečinok, kde chceš uložiť PDF.", QDir::homePath(), QFileDialog::ShowDirsOnly);
    if(folder == "")
        return;

    QString pdfFilePath = folder + "/" + pdfFileName + ".pdf";

    PdfCreator pdfCreator;
    pdfCreator.createPdf(*this, pdfFilePath);
}
