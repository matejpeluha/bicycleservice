#include "infodialog.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDir>
#include <QDebug>
#include <QMessageBox>
#include <QUrl>
#include <QDesktopServices>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    this->ui->setupUi(this);

    setColumnSizes();

    this->databaseHandler = new DatabaseHandler(this->ui->table);
    loadData();

    this->lastId = getLastId();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setColumnSizes(){
    this->ui->table->horizontalHeader()->setSectionResizeMode(8, QHeaderView::Fixed);
    this->ui->table->setColumnWidth(0, 90);

    for(int index = 1; index < 8; index ++)
        this->ui->table->horizontalHeader()->setSectionResizeMode(index, QHeaderView::Stretch);

    this->ui->table->horizontalHeader()->setSectionResizeMode(8, QHeaderView::Fixed);
    this->ui->table->setColumnWidth(7, 90);

    this->ui->table->horizontalHeader()->setSectionResizeMode(9, QHeaderView::Fixed);
    this->ui->table->setColumnWidth(8, 90);

    this->ui->table->setSelectionBehavior(QAbstractItemView::SelectRows);
    this->ui->table->setFocusPolicy(Qt::NoFocus);
}

void MainWindow::loadData(){
    this->databaseHandler->openDatabase(QIODevice::ReadOnly);

    this->databaseHandler->loadData();
    this->databaseHandler->createBackUp();

    this->databaseHandler->closeDatabase();
}

int MainWindow::getLastId(){
    int numberOfRows = this->ui->table->rowCount() - 1;
    if(numberOfRows < 0)
        return 0;

    bool ok;
    int lastId = this->ui->table->item(numberOfRows, this->ID_COLUMN_INDEX)->text().toInt(&ok);

    if(!ok)
        return 500000;

    return lastId;
}

void MainWindow::on_addButton_clicked()
{
    this->addDialog = new AddDialog(this);
    this->addDialog->setLabel("Nový záznam");
    this->addDialog->setId(this->lastId + 1);

    connect(this->addDialog, SIGNAL(closeSignal(void)), this, SLOT(deleteAddDialog(void)));
    connect(this->addDialog, SIGNAL(finishSignal(QString)), this, SLOT(writeInfoToTable(QString)));

    this->addDialog->show();
}

void MainWindow::deleteAddDialog(){
    delete this->addDialog;
}

void MainWindow::writeInfoToTable(QString id){
    int numberOfRows = this->ui->table->rowCount();
    this->ui->table->setRowCount(numberOfRows + 1);

    this->lastId = id.toInt();

    writeIdToTable(numberOfRows);
    writeNameToTable(numberOfRows);
    writePhoneToTable(numberOfRows);
    writeAdressToTable(numberOfRows);
    writeBicycleTypeToTable(numberOfRows);
    writeBicycleNumberToTable(numberOfRows);
    writeServiceToTable(numberOfRows);
    writeDateToTable(numberOfRows);
    writePriceToTable(numberOfRows);

    setDetailColumn(numberOfRows);
    setRemoveColumn(numberOfRows);

    writeNewRowToDatabase();

    deleteAddDialog();
}

void MainWindow::writeNewRowToDatabase(){
    this->databaseHandler->openDatabase(QIODevice::WriteOnly | QIODevice::Append);

    this->databaseHandler->appendRow(*this->addDialog);
    this->databaseHandler->createBackUp();

    this->databaseHandler->closeDatabase();
}

void MainWindow::writeIdToTable(int rowIndex){
    QString id = this->addDialog->getId();
    QTableWidgetItem* item = new QTableWidgetItem(id);
    item->setFlags(item->flags() & (~Qt::ItemIsEditable));
    this->ui->table->setItem(rowIndex, this->ID_COLUMN_INDEX, item);
}

void MainWindow::writeNameToTable(int rowIndex){
    QString name = this->addDialog->getName();
    QTableWidgetItem* item = new QTableWidgetItem(name);
    item->setFlags(item->flags() & (~Qt::ItemIsEditable));
    this->ui->table->setItem(rowIndex, this->NAME_COLUMN_INDEX, item);
}

void MainWindow::writePhoneToTable(int rowIndex){
    QString name = this->addDialog->getPhoneNumber();
    QTableWidgetItem* item = new QTableWidgetItem(name);
    item->setFlags(item->flags() & (~Qt::ItemIsEditable));
    this->ui->table->setItem(rowIndex, this->PHONE_COLUMN_INDEX, item);
}

void MainWindow::writeAdressToTable(int rowIndex){
    QString name = this->addDialog->getAdress();
    QTableWidgetItem* item = new QTableWidgetItem(name);
    item->setFlags(item->flags() & (~Qt::ItemIsEditable));
    this->ui->table->setItem(rowIndex, this->ADRESS_COLUMN_INDEX, item);
}

void MainWindow::writeBicycleTypeToTable(int rowIndex){
    QString name = this->addDialog->getBicycleType();
    QTableWidgetItem* item = new QTableWidgetItem(name);
    item->setFlags(item->flags() & (~Qt::ItemIsEditable));
    this->ui->table->setItem(rowIndex, this->BICYCLE_TYPE_COLUMN_INDEX, item);
}

void MainWindow::writeBicycleNumberToTable(int rowIndex){
    QString name = this->addDialog->getBicycleNumber();
    QTableWidgetItem* item = new QTableWidgetItem(name);
    item->setFlags(item->flags() & (~Qt::ItemIsEditable));
    this->ui->table->setItem(rowIndex, this->BICYCLE_NUMBER_COLUMN_INDEX, item);
}

void MainWindow::writeServiceToTable(int rowIndex){
    QString name = this->addDialog->getService();
    QTableWidgetItem* item = new QTableWidgetItem(name);
    item->setFlags(item->flags() & (~Qt::ItemIsEditable));
    this->ui->table->setItem(rowIndex, this->SERVICE_COLUMN_INDEX, item);
}

void MainWindow::writeDateToTable(int rowIndex){
    QString name = this->addDialog->getDate();
    QTableWidgetItem* item = new QTableWidgetItem(name);
    item->setFlags(item->flags() & (~Qt::ItemIsEditable));
    this->ui->table->setItem(rowIndex, this->DATE_COLUMN_INDEX, item);
}

void MainWindow::writePriceToTable(int rowIndex){
    QString name = this->addDialog->getPrice();
    QTableWidgetItem* item = new QTableWidgetItem(name);
    item->setFlags(item->flags() & (~Qt::ItemIsEditable));
    this->ui->table->setItem(rowIndex, this->PRICE_COLUMN_INDEX, item);
}

void MainWindow::setRemoveColumn(int rowIndex){
    QIcon icon;
    QSize sz(16, 16);

    icon.addPixmap(style()->standardIcon(QStyle::SP_TrashIcon).pixmap(sz), QIcon::Normal);

    QTableWidgetItem *iconItem = new QTableWidgetItem();
    iconItem->setText("Zmazať");
    iconItem->setIcon(icon);
    iconItem->setFlags(iconItem->flags() & (~Qt::ItemIsEditable));

    this->ui->table->setItem(rowIndex, this->REMOVE_COLUMN_INDEX, iconItem);
}

void MainWindow::setDetailColumn(int rowIndex){
    QIcon icon;
    QSize sz(16, 16);

    icon.addPixmap(style()->standardIcon(QStyle::SP_FileDialogContentsView).pixmap(sz), QIcon::Normal);

    QTableWidgetItem *iconItem = new QTableWidgetItem();
    iconItem->setText("Otvoriť");
    iconItem->setIcon(icon);
    iconItem->setFlags(iconItem->flags() & (~Qt::ItemIsEditable));

    this->ui->table->setItem(rowIndex, this->DETAIL_COLUMN_INDEX, iconItem);
}

void MainWindow::on_table_cellDoubleClicked(int row, int column)
{
    if(column == this->REMOVE_COLUMN_INDEX)
        removeRow(row);
    if(column == this->DETAIL_COLUMN_INDEX)
        openDetail(row);
}

void MainWindow::removeRow(int rowIndex){
    QMessageBox::StandardButton reply =
            QMessageBox::question(this, "Zmazanie záznamu", "Naozaj si prajete záznam zmazať?",
                                  QMessageBox::Yes | QMessageBox::No);

    if(reply == QMessageBox::Yes){
        removeRowFromDatabase(rowIndex);
        this->ui->table->removeRow(rowIndex);
    }

}

void MainWindow::removeRowFromDatabase(int rowIndex){
    InfoDialog* infoDialog = new InfoDialog(this);
    infoDialog->openWithInfo("Maže sa riadok");

    this->databaseHandler->openDatabase(QIODevice::ReadWrite);
    this->databaseHandler->removeRow(rowIndex);
    this->databaseHandler->createBackUp();
    this->databaseHandler->closeDatabase();

    infoDialog->close();
}


void MainWindow::openDetail(int rowIndex){
    this->addDialog = new AddDialog(this);
    setAddDialog(rowIndex);

    connect(this->addDialog, SIGNAL(closeSignal(void)), this, SLOT(deleteAddDialog(void)));
    connect(this->addDialog, SIGNAL(finishSignal(QString)), this, SLOT(editChanged(QString)));

    this->addDialog->show();
}

void MainWindow::setAddDialog(int rowIndex){
    this->addDialog->setLabel("Detail záznamu");

    QTableWidget* table = this->ui->table;
    QString id = table->item(rowIndex, this->ID_COLUMN_INDEX)->text();
    this->addDialog->setId(id.toInt());

    QString name = table->item(rowIndex, this->NAME_COLUMN_INDEX)->text();
    this->addDialog->setName(name);

    QString phoneNumber = table->item(rowIndex, this->PHONE_COLUMN_INDEX)->text();
    this->addDialog->setPhoneNumber(phoneNumber);

    QString adress = table->item(rowIndex, this->ADRESS_COLUMN_INDEX)->text();
    this->addDialog->setAdress(adress);

    QString bicycleType = table->item(rowIndex, this->BICYCLE_TYPE_COLUMN_INDEX)->text();
    this->addDialog->setBicycleType(bicycleType);

    QString bicycleNumber = table->item(rowIndex, this->BICYCLE_NUMBER_COLUMN_INDEX)->text();
    this->addDialog->setBicycleNumber(bicycleNumber);

    QString service = table->item(rowIndex, this->SERVICE_COLUMN_INDEX)->text();
    this->addDialog->setService(service);

    QString date = table->item(rowIndex, this->DATE_COLUMN_INDEX)->text();
    this->addDialog->setDate(date);

    this->databaseHandler->openDatabase(QIODevice::ReadWrite);
    this->databaseHandler->loadComponents(*this->addDialog, rowIndex);
    this->databaseHandler->closeDatabase();

    QString price = table->item(rowIndex, this->PRICE_COLUMN_INDEX)->text();
    this->addDialog->setPrice(price);
}


void MainWindow::editChanged(QString id){
    int rowIndex;
    QString readId;
    for(rowIndex = 0; rowIndex < this->ui->table->rowCount(); rowIndex++){
        readId = this->ui->table->item(rowIndex, this->ID_COLUMN_INDEX)->text();
        if(readId.compare(id) == 0)
            break;
    }

    if(rowIndex >= this->ui->table->rowCount()){
        deleteAddDialog();
        return;
    }

    writeNameToTable(rowIndex);
    writePhoneToTable(rowIndex);
    writeAdressToTable(rowIndex);
    writeBicycleTypeToTable(rowIndex);
    writeBicycleNumberToTable(rowIndex);
    writeServiceToTable(rowIndex);
    writeDateToTable(rowIndex);
    writePriceToTable(rowIndex);

    editRowInDatabase();

    deleteAddDialog();
}


void MainWindow::editRowInDatabase(){
    this->databaseHandler->openDatabase(QIODevice::ReadWrite);

    this->databaseHandler->editRow(*this->addDialog);
    this->databaseHandler->createBackUp();

    this->databaseHandler->closeDatabase();
}


void MainWindow::on_nameFilter_textChanged(const QString &name)
{
    QString service = this->ui->serviceFilter->text();
    this->ui->table->setRowCount(0);

    this->databaseHandler->openDatabase(QIODevice::ReadOnly);

    this->databaseHandler->loadData(name, service);

    this->databaseHandler->closeDatabase();

}

void MainWindow::on_serviceFilter_textChanged(const QString &service)
{
    QString name = this->ui->nameFilter->text();
    this->ui->table->setRowCount(0);

    this->databaseHandler->openDatabase(QIODevice::ReadOnly);

    this->databaseHandler->loadData(name, service);

    this->databaseHandler->closeDatabase();
}

void MainWindow::on_databaseButton_clicked()
{
    QString path = QDir::currentPath() + "/data";

    if(!fileExists(path)){
        QMessageBox::warning(this, "Databáza", "Cesta k databáze neexistuje");
        return;
    }

    QUrl urlDirection = QUrl::fromLocalFile(path);
    QDesktopServices::openUrl( urlDirection );
}

void MainWindow::on_backUpButton_clicked()
{
    QString path = QDir::currentPath().split("/").first() + "/bicycle_data";

    if(!fileExists(path)){
        QMessageBox::warning(this, "Databáza", "Cesta k zálohe databázy neexistuje");
        return;
    }

    QUrl urlDirection = QUrl::fromLocalFile(path);
    QDesktopServices::openUrl( urlDirection );
}

bool MainWindow::fileExists(QString& path) {
    QFileInfo checkFile(path);

    return checkFile.exists();
}
