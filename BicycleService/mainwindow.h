#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "adddialog.h"
#include "databasehandler.h"

#include <QFile>
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    bool fileExists(QString &path);
private slots:
    void on_addButton_clicked();

    void deleteAddDialog();
    void writeInfoToTable(QString id);
    void on_table_cellDoubleClicked(int row, int column);

    void editChanged(QString id);
    void on_nameFilter_textChanged(const QString &text);
    void on_serviceFilter_textChanged(const QString &service);
    void on_databaseButton_clicked();

    void on_backUpButton_clicked();
private:
    Ui::MainWindow *ui;
    AddDialog* addDialog;
    DatabaseHandler* databaseHandler;

    int lastId;

    const int ID_COLUMN_INDEX = 0;
    const int NAME_COLUMN_INDEX = 1;
    const int PHONE_COLUMN_INDEX = 2;
    const int ADRESS_COLUMN_INDEX = 3;
    const int BICYCLE_TYPE_COLUMN_INDEX = 4;
    const int BICYCLE_NUMBER_COLUMN_INDEX = 5;
    const int SERVICE_COLUMN_INDEX = 6;
    const int DATE_COLUMN_INDEX = 7;
    const int PRICE_COLUMN_INDEX = 8;
    const int DETAIL_COLUMN_INDEX = 9;
    const int REMOVE_COLUMN_INDEX = 10;


    void setColumnSizes();
    void loadData();
    void writeNameToTable(int rowIndex);

    void writePhoneToTable(int rowIndex);
    void writeAdressToTable(int rowIndex);
    void writeBicycleTypeToTable(int rowIndex);
    void writeBicycleNumberToTable(int rowIndex);
    void writeDateToTable(int rowIndex);
    void writePriceToTable(int rowIndex);
    void removeRow(int rowIndex);
    void setRemoveColumn(int rowIndex);
    void writeServiceToTable(int rowIndex);
    void writeNewRowToDatabase();
    void removeRowFromDatabase(int rowIndex);
    void setDetailColumn(int rowIndex);
    void openDetail(int rowIndex);
    void setAddDialog(int rowIndex);
    int getLastId();
    void writeIdToTable(int rowIndex);
    void editRowInDatabase();
};
#endif // MAINWINDOW_H
