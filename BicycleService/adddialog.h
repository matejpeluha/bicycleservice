#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include "addcomponentdialog.h"

#include <QDialog>
#include <QDoubleSpinBox>
#include <QTableWidget>

namespace Ui {
class AddDialog;
}

class AddDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddDialog(QWidget *parent = nullptr);
    ~AddDialog();

    void closeEvent(QCloseEvent *event);

    QString getName();
    QString getPhoneNumber();
    QString getAdress();
    QString getBicycleType();
    QString getBicycleNumber();
    QString getService();
    QString getPrice();
    QString getDate();

    void setLabel(const QString &labelName);
    void setName(const QString &name);
    void setPhoneNumber(const QString &phoneNumber);
    void setAdress(const QString &adress);
    void setBicycleType(const QString &bicycleType);
    void setBicycleNumber(const QString &bicycleNumber);
    void setService(const QString &service);
    void setPrice(const QString& price);
    void setDate(const QString &date);
    QString getId();
    void setId(int id);
    QTableWidget& getComponentsTable();
    QString getManWorkPrice();
    void setManWorkPrice(QString &manWorkPrice);
    void showTotalPrice();
    void addTotalPrice(QString &price);
    void addComponent(QString &componentName, double componentPrice);
    void addComponent(int componentCount, QString &componentName, double componentPrice);
private slots:
    void on_addComponentButton_clicked();

    void on_table_cellDoubleClicked(int row, int column);

private:
    Ui::AddDialog *ui;
    AddComponentDialog* addComponentDialog;
    double manWorkPrice;
    double totalPrice;

    const int COUNT_COLUMN_INDEX = 0;
    const int COMPONENT_COLUMN_INDEX = 1;
    const int PRICE_COLUMN_INDEX = 2;
    const int REMOVE_COLUMN_INDEX = 3;

    void setColumnSizes();
    void setRemoveColumn(int rowIndex);
    void removeRow(int rowIndex);
    void setPriceColumn(int rowIndex, double price);

    void setComponentColumn(int rowIndex, QString componentName);

    void setDate();
    void setCountColumn(int rowIndex, int value = 1);
private slots:
        void addComponentSlot(QString componentName, double componentPrice);
        void deleteAddComponentDialog();
        void on_manWorkPriceInput_valueChanged(double manWorkPrice);
        void on_confirmButton_clicked();

        void on_pdfButton_clicked();

        void valueChangedSlot();
signals:
        void closeSignal();
        void finishSignal(QString id);
};

#endif // ADDDIALOG_H
