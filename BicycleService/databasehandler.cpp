#include "databasehandler.h"

#include <QDir>
#include <QTextStream>

#include <QDebug>

DatabaseHandler::DatabaseHandler(QTableWidget* table)
{
    this->table = table;
}


void DatabaseHandler::openDatabase(QIODevice::OpenMode mode){
    this->dataPath = QDir::currentPath() + "/data/zaznamy.csv";

    this->csvFile = new QFile(this->dataPath);
    this->csvFile->open(mode);

    this->csvStream = new QTextStream(this->csvFile);
}

void DatabaseHandler::loadData(const QString& nameFilter, const QString& serviceFilter){
    QString line, name, service;
    QStringList splitLine;

    while(!this->csvStream->atEnd()){
        line = this->csvStream->readLine();
        splitLine = line.split(";");
        name = splitLine.at(1);
        service = splitLine.at(6);

        if(name.startsWith(nameFilter) && service.startsWith(serviceFilter))
            writeLineToTable(line);
    }
}


void DatabaseHandler::writeLineToTable(QString& line){
    int numberOfRows = this->table->rowCount();
    this->table->setRowCount(numberOfRows + 1);

    for(int columnIndex = 0; columnIndex < 9; columnIndex++)
        writeInfoToTableCell(line, columnIndex);

    setDetailColumn(numberOfRows);
    setRemoveColumn(numberOfRows);
}

void DatabaseHandler::writeInfoToTableCell(QString& line, int columnIndex){
    int lastRowIndex = this->table->rowCount() - 1;

    QString info = line.split(";").at(columnIndex);
    QTableWidgetItem* item = new QTableWidgetItem(info);
    item->setFlags(item->flags() & (~Qt::ItemIsEditable));

    this->table->setItem(lastRowIndex, columnIndex, item);
}

void DatabaseHandler::setRemoveColumn(int rowIndex){
    QIcon icon;
    QSize sz(16, 16);

    icon.addPixmap(this->table->style()->standardIcon(QStyle::SP_TrashIcon).pixmap(sz), QIcon::Normal);

    QTableWidgetItem *iconItem = new QTableWidgetItem();
    iconItem->setText("Zmazať");
    iconItem->setIcon(icon);
    iconItem->setFlags(iconItem->flags() & (~Qt::ItemIsEditable));

    this->table->setItem(rowIndex, this->REMOVE_COLUMN_INDEX, iconItem);
}

void DatabaseHandler::setDetailColumn(int rowIndex){
    QIcon icon;
    QSize sz(16, 16);

    icon.addPixmap(this->table->style()->standardIcon(QStyle::SP_FileDialogContentsView).pixmap(sz), QIcon::Normal);

    QTableWidgetItem *iconItem = new QTableWidgetItem();
    iconItem->setText("Otvoriť");
    iconItem->setIcon(icon);
    iconItem->setFlags(iconItem->flags() & (~Qt::ItemIsEditable));

    this->table->setItem(rowIndex, this->DETAIL_COLUMN_INDEX, iconItem);
}

void DatabaseHandler::appendRow(AddDialog &addDialog)
{
    *this->csvStream << addDialog.getId().replace(";", ",") << ";";
    *this->csvStream << addDialog.getName().replace(";", ",") << ";";
    *this->csvStream << addDialog.getPhoneNumber().replace(";", ",") << ";";
    *this->csvStream << addDialog.getAdress().replace(";", ",") << ";";
    *this->csvStream << addDialog.getBicycleType().replace(";", ",") << ";";
    *this->csvStream << addDialog.getBicycleNumber().replace(";", ",") << ";";
    *this->csvStream << addDialog.getService().replace(";", ",") << ";";
    *this->csvStream << addDialog.getDate().replace(";", ",") << ";";
    *this->csvStream << addDialog.getPrice().replace(";", ",") << ";";

    *this->csvStream << addDialog.getManWorkPrice() << ";";

    QTableWidget& componentsTable = addDialog.getComponentsTable();
    int numberOfRows = componentsTable.rowCount();

    for(int rowIndex = 0; rowIndex < numberOfRows; rowIndex++){
        QSpinBox* spinBox = (QSpinBox*)componentsTable.cellWidget(rowIndex,0);
        QString componentCount = QString::number(spinBox->value());
        QString componentName = componentsTable.item(rowIndex, 1)->text().replace(";", ",").remove(":");
        QString componentPrice = componentsTable.item(rowIndex, 2)->text().replace(";", ",");

        *this->csvStream << componentCount << ":" << componentName << ":" << componentPrice;

        if(rowIndex != numberOfRows - 1 )
            *this->csvStream << ",";
    }

    *this->csvStream << "\n";

}


void DatabaseHandler::removeRow(int rowIndex){
    QString line;
    QString fileContent;
    QString id = this->table->item(rowIndex, this->ID_COLUMN_INDEX)->text();
    QString readId;

    while(!this->csvStream->atEnd()){
        line = this->csvStream->readLine() + "\n";
        readId = line.split(";").first();

        if(readId.compare(id)){
            fileContent += line;
        }
    }


    closeDatabase();
    openDatabase(QIODevice::WriteOnly | QIODevice::Truncate);

    *this->csvStream << fileContent;
}

void DatabaseHandler::loadComponents(AddDialog& addDialog, int rowIndex){
    QString id = this->table->item(rowIndex, this->ID_COLUMN_INDEX)->text();
    QString readId;
    QString line;
    QString components;
    QString manWorkPrice;

    while(!this->csvStream->atEnd()){
        line = this->csvStream->readLine();
        QStringList splitLine = line.split(";");
        readId = splitLine.first();

        if(readId.compare(id) == 0){
            components = splitLine.at(10);
            manWorkPrice = splitLine.at(9);
            break;
        }
    }

    if(components.length() > 0)
        loadComponentsToTable(addDialog, components);

    addDialog.setManWorkPrice(manWorkPrice);
}

void DatabaseHandler::loadComponentsToTable(AddDialog& addDialog, QString& components){
    QStringList componentsList = components.split(",");
    QStringList oneComponent;
    QString component;
    double price;
    int componentCount;

    for(int rowIndex = 0; rowIndex < componentsList.length(); rowIndex++){
        oneComponent = componentsList.at(rowIndex).split(":");
        componentCount = oneComponent.first().toInt();
        component = oneComponent.at(1);
        price = oneComponent.last().remove("€").toDouble();
        addDialog.addComponent(componentCount, component, price);
    }
}

void DatabaseHandler::editRow(AddDialog& addDialog){
    QString id = addDialog.getId();
    QString line;
    QString readId;
    QStringList splitLine;
    QString newContent;


   while(!this->csvStream->atEnd()){
        line = this->csvStream->readLine();
        splitLine = line.split(";");
        readId = splitLine.first();

        if(readId.compare(id) !=0)
            newContent += line + "\n";
        else
            addEditedRow(addDialog, newContent);
    }

    closeDatabase();
    openDatabase(QIODevice::WriteOnly | QIODevice::Truncate);

    *this->csvStream << newContent;
}

void DatabaseHandler::addEditedRow(AddDialog& addDialog, QString& newContent){
    newContent += addDialog.getId().replace(";", ",") + ";";
    newContent += addDialog.getName().replace(";", ",") + ";";
    newContent += addDialog.getPhoneNumber().replace(";", ",") + ";";
    newContent += addDialog.getAdress().replace(";", ",") + ";";
    newContent += addDialog.getBicycleType().replace(";", ",") + ";";
    newContent += addDialog.getBicycleNumber().replace(";", ",") + ";";
    newContent += addDialog.getService().replace(";", ",") + ";";
    newContent += addDialog.getDate().replace(";", ",") + ";";
    newContent += addDialog.getPrice().replace(";", ",") + ";";

    newContent += addDialog.getManWorkPrice() + ";";

    QTableWidget& componentsTable = addDialog.getComponentsTable();
    int numberOfRows = componentsTable.rowCount();
    for(int rowIndex = 0; rowIndex < numberOfRows; rowIndex++){
        QSpinBox* spinBox = (QSpinBox*)componentsTable.cellWidget(rowIndex,0);
        QString componentCount = QString::number(spinBox->value());
        QString componentName = componentsTable.item(rowIndex, 1)->text().replace(";", ",").remove(":");
        QString componentPrice = componentsTable.item(rowIndex, 2)->text().replace(";", ",");

        newContent += componentCount + ":" + componentName + ":" + componentPrice;

        if(rowIndex != numberOfRows - 1 )
            newContent += ",";
    }

    newContent += "\n";
}

void DatabaseHandler::closeDatabase()
{
    delete this->csvStream;

    this->csvFile->close();

    createBackUp();
    delete this->csvFile;
}

void DatabaseHandler::createBackUp(){
    QString copyPath = this->dataPath.split("/").first() + "/bicycle_data";
    QDir dir;
    if(!dir.exists())
        dir.mkpath(copyPath);;

    QString backUpPath = copyPath + "/zaznamy.csv";

    QFile* file = new QFile(backUpPath);
    file->remove();

    this->csvFile->copy(backUpPath);
}
