#include "infodialog.h"
#include "ui_infodialog.h"

InfoDialog::InfoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InfoDialog)
{
    ui->setupUi(this);
    setWindowFlags(windowFlags() & (~Qt::WindowSystemMenuHint) & (~Qt::WindowCloseButtonHint) & (~Qt::WindowContextHelpButtonHint));
}

InfoDialog::~InfoDialog()
{
    delete ui;
}

void InfoDialog::openWithInfo(const QString& info){
    this->ui->label->setText(info);
    this->open();
}
